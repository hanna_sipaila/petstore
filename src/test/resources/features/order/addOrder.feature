Feature: Add order

Scenario: Positive - add order
   Given the following pet:
   | id | name     | status        |
   | 1  | test     | available     |
   When I add pet
   Then response status 200
   Given the following order:
   | id | petId   | quantity | shipDate                | status | complete |
   | 1  | -1      | 1        |2020-10-25T18:19:29.435Z | placed | true     |
   When I add order
   Then response status 200
   When get order by id 1
   Then verify received order has following details:
   | id | petId | quantity | shipDate                | status | complete |
   | 1  | 1     | 1        |2020-10-25T18:19:29.435Z | placed | true     |


