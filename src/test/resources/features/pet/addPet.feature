Feature: Add pet

Scenario: Positive - add pet using dataTable
    Given the following pet:
      | id | name      | status        |
      | 10 | kotya     | available     |
    When I add pet
    Then response status 200
    When get pet by id 10
    Then verify pet object:
      | id | name      | status        |
      | 10 | kotya     | available     |

Scenario: Positive - add pet using data from file
    Given pet with name "doggie2"
    When I add pet
    Then response status 200
    Then verify received pet with name "doggie2"