Feature: Search pet

Scenario: Get pet by id = 1
  When get pet by id 1
  Then verify received pet object is not empty

Scenario: Get pet by id = 99999999999
  When get pet by id 99999999999
  Then verify received error with code 1, status "error", message "Pet not found"

