Feature: Delete pet

Scenario: Delete pet
  Given pet with name "doggie2"
  When I add pet
  Then response status 200
  When delete pet
  When get added pet
  Then verify received error with code 1, status "error", message "Pet not found"

