Feature: Update pet by id

Scenario: Positive update pet
  Given pet with name "new pet"
  When I add pet
  When get added pet
  Then verify received pet with name "new pet"
  When update pet with id 3 with name "Updated name" and status "Unavailable"
  Then get pet by id 3
  Then verify received pet with name "Updated name"

Scenario: Negative update pet with nonexistent id
   When update pet with id 878384758748574857 with name "fail" and status "fail"
   Then response status 404