package com.petstore.stepdefinitions;

import com.petstore.steps.PetSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import net.thucydides.core.annotations.Steps;

import java.io.IOException;

public class PetStepDefinitions{

    @Steps
    PetSteps petSteps;

    @Given("the following pet:")
    public void the_following_pet(DataTable dataTable){
        petSteps.setPet(dataTable);
    }

    @Given("pet with name {string}")
    public void pet_with_name(String name) throws IOException {
        petSteps.setPet(name);
    }

    @When("I add pet")
    public void add_pet() throws IOException {
        petSteps.addPet();
    }

    @When("get pet by id {long}")
    public void get_pet_by_id(Long id){
        petSteps.getPetById(id);
    }

    @Then("verify pet object:")
    public void verify_pet_object(DataTable expectedPet){
       petSteps.verifyPet(expectedPet);
    }

    @Then("verify received pet with name {string}")
    public void verify_received_pet_object_with_name(String name) throws IOException {
       petSteps.verifyPet(name);
    }

    @When("delete pet")
    public void delete_pet(){
        petSteps.deletePet();
    }

    @When("get added pet")
    public void get_added_pet(){
        petSteps.getPet();
    }

    @Then("verify received pet object is not empty")
    public void verify_received_pet_object_is_not_empty() {
      petSteps.verifyPetObjectIsNotEmpty();
    }

    @When("update pet with id {long} with name {string} and status {string}")
    public void update_pet(Long id, String name, String status) {
        petSteps.updatePet(id, name, status);
    }

}
