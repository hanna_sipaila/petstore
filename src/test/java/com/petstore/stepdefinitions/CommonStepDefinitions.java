package com.petstore.stepdefinitions;

import com.petstore.steps.CommonSteps;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class CommonStepDefinitions {

    @Steps
    CommonSteps commonSteps;

    @Then("response status {int}")
    public void verify_response_status_code(int statusCode) {
        commonSteps.verifyResponseStatusCode(statusCode);
    }

    @Then("verify received error with code {int}, status {string}, message {string}")
    public void receive_error(int code, String status, String message) {
       commonSteps.verifyErrorResponse(code, status, message);
    }

}
