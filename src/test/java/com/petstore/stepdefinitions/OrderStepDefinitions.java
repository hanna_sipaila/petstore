package com.petstore.stepdefinitions;

import com.petstore.steps.OrderSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import net.thucydides.core.annotations.Steps;

import java.io.IOException;

public class OrderStepDefinitions{

    @Steps
    OrderSteps orderSteps;

    @Given("the following order:")
    public void the_following_order(DataTable order){
       orderSteps.setOrder(order);
    }

    @When("I add order")
    public void add_order() throws IOException {
        orderSteps.addOrder();
    }

    @When("get order by id {long}")
    public void get_order(Long id) throws IOException {
        orderSteps.getOrderById(id);
    }

    @Then("verify received order has following details:")
    public void verify_order(DataTable expectedOrder) throws IOException {
        orderSteps.verifyOrder(expectedOrder);
    }

}
