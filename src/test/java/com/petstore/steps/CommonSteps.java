package com.petstore.steps;

import com.petstore.model.ApiResponse;
import io.restassured.response.Response;

import static org.assertj.core.api.Assertions.assertThat;

public class CommonSteps extends AbstractSteps {

    public void verifyResponseStatusCode(int code){
        Response response = testContext().getResponse();
        assertThat(response.getStatusCode()).isEqualTo(code);
    }

    public void verifyErrorResponse(Integer code, String status, String message){
        ApiResponse actualError = testContext().getResponse().as(ApiResponse.class);
        assertThat(actualError.getCode()).isEqualTo(code);
        assertThat(actualError.getType()).isEqualTo(status);
        assertThat(actualError.getMessage()).isEqualTo(message);
    }

}
