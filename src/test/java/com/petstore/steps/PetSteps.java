package com.petstore.steps;

import com.petstore.controller.PetController;
import com.petstore.model.pet.Pet;
import com.petstore.utility.testdatamanager.PetDataManager;
import io.cucumber.datatable.DataTable;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PetSteps extends CommonSteps {

    PetDataManager dataManager = new PetDataManager();

    @Steps
    PetController petController;

    public void addPet() throws IOException {
        Pet pet = testContext().getPayload(Pet.class);
        Response response = petController.addPet(pet);
        testContext().setResponse(response);
        testContext().setPayload(response.as(Pet.class));
    }

    public void setPet(DataTable dataTable){
        Pet pet = dataManager.convertMapToObject(dataTable.asMaps().get(0));
        testContext().setPayload(pet);
    }

    //set pet using data from file
    public void setPet(String name) throws IOException {
        List<Pet> allPetsFromFile = dataManager.readObjects();
        Pet pet = dataManager.findObjectByName(allPetsFromFile, name);
        testContext().setPayload(pet);
    }

    public void getPetById(Long id){
        Response response = petController.getPet(id);
        Pet pet = response.as(Pet.class);
        testContext().setPayload(pet);
        testContext().setResponse(response);
    }

    public void verifyPet(DataTable dataTable){
        Pet expectedPet = dataManager.convertMapToObject(dataTable.asMaps().get(0));
        Pet actualPet = testContext().getPayload(Pet.class);
        assertThat(expectedPet).isEqualTo(actualPet);
    }

    public void verifyPet(String name) throws IOException {
        List<Pet> allPetsFromFile = dataManager.readObjects();
        Pet expectedPet = dataManager.findObjectByName(allPetsFromFile, name);
        Pet actualPet = testContext().getPayload(Pet.class);
        assertThat(expectedPet).isEqualTo(actualPet);
    }

    public void deletePet(){
        Long petId = testContext().getPayload(Pet.class).getId();
        Response response = petController.deletePet(petId);
        testContext().setResponse(response);
    }

    //get pet using petId from test context
    public void getPet(){
        Long petId = testContext().getPayload(Pet.class).getId();
        Response response = petController.getPet(petId);
        testContext().setResponse(response);
    }

    public void verifyPetObjectIsNotEmpty(){
        Pet pet = testContext().getPayload(Pet.class);
        assertThat(pet.getId() != null).isTrue();
        assertThat(pet.getName()).isNotBlank();
        assertThat(pet.getStatus()).isNotBlank();
        assertThat(pet.getCategory()).isNotNull();
    }

    public void updatePet(Long id, String name, String status){
        Response response = petController.updatePet(id, name, status);
        testContext().setResponse(response);
    }

}
