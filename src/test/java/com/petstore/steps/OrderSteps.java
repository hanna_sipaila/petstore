package com.petstore.steps;

import com.petstore.controller.OrderController;
import com.petstore.model.order.Order;
import com.petstore.model.pet.Pet;
import com.petstore.utility.testdatamanager.OrderDataManager;
import io.cucumber.datatable.DataTable;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class OrderSteps extends CommonSteps {
    OrderDataManager dataManager = new OrderDataManager();

    @Steps
    OrderController controller;

    public void setOrder(DataTable dataTable){
       Pet pet = testContext().getPayload(Pet.class);
       Order order = dataManager.convertMapToObject(dataTable.asMaps().get(0));
       order.setPetId(pet.getId());
       testContext().setPayload(order);
    }

    public void addOrder() throws IOException {
        Order order = testContext().getPayload(Order.class);
        Response response = controller.addOrder(order);
        testContext().setResponse(response);
    }

    public void getOrderById(Long id){
       Response response = controller.getOrder(id);
       Order order = response.as(Order.class);
       testContext().setPayload(order);
    }

    public void verifyOrder(DataTable dataTable){
        Order expectedOrder = dataManager.convertMapToObject(dataTable.asMaps().get(0));
        Order actualOrder = testContext().getPayload(Order.class);
        assertThat(expectedOrder).isEqualTo(actualOrder);
    }

}
