package com.petstore.apitests.order;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/features/order/addOrder.feature",
        glue = "com.petstore.stepdefinitions"
)
public class AddOrderTest {
}
