package com.petstore.model.order;

import com.fasterxml.jackson.annotation.JsonFormat;

public class Order {
    private Long id;
    private Long petId;
    private Integer quantity;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-ddHH:mm:ss.SSSZ")
    private String shipDate;
    private String status;
    private Boolean complete;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPetId() {
        return petId;
    }

    public void setPetId(Long petId) {
        this.petId = petId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getShipDate() {
        return shipDate;
    }

    public void setShipDate(String shipDate) {
        this.shipDate = shipDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean isComplete() {
        return complete;
    }

    public void setComplete(Boolean complete) {
        this.complete = complete;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        final Order order = (Order) obj;
        if ((this.id == null) ? (order.id != null) : !this.id.equals(order.id)) {
            return false;
        }
        if ((this.petId == null) ? (order.petId != null) : !this.petId.equals(order.petId)) {
            return false;
        }
        if ((this.quantity == null) ? (order.quantity != null) : !this.quantity.equals(order.quantity)) {
            return false;
        }
        if ((this.status == null) ? (order.status != null) : !this.status.equals(order.status)) {
            return false;
        }
        if ((this.complete == null) ? (order.complete != null) : !this.complete.equals(order.complete)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 53 * hash + (this.petId != null ? this.petId.hashCode() : 0);
        hash = 53 * hash + (this.quantity != null ? this.quantity.hashCode() : 0);
        hash = 53 * hash + (this.shipDate != null ? this.shipDate.hashCode() : 0);
        hash = 53 * hash + (this.status != null ? this.status.hashCode() : 0);
        hash = 53 * hash + (this.complete != null ? this.complete.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", petId=" + petId +
                ", quantity=" + quantity +
                ", shipDate='" + shipDate + '\'' +
                ", status='" + status + '\'' +
                ", complete=" + complete +
                '}';
    }
}
