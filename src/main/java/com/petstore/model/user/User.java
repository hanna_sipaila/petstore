package com.petstore.model.user;

public class User {
    private Long id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String phone;
    private Integer userStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        final User user = (User) obj;
        if ((this.id == null) ? (user.id != null) : !this.id.equals(user.id)) {
            return false;
        }
        if ((this.username == null) ? (user.username != null) : !this.username.equals(user.username)) {
            return false;
        }
        if ((this.firstName == null) ? (user.firstName != null) : !this.firstName.equals(user.firstName)) {
            return false;
        }
        if ((this.lastName == null) ? (user.lastName != null) : !this.lastName.equals(user.lastName)) {
            return false;
        }
        if ((this.email == null) ? (user.email != null) : !this.email.equals(user.email)) {
            return false;
        }
        if ((this.password == null) ? (user.password != null) : !this.password.equals(user.password)) {
            return false;
        }
        if ((this.phone == null) ? (user.phone != null) : !this.phone.equals(user.phone)) {
            return false;
        }
        if ((this.userStatus == null) ? (user.userStatus != null) : !this.userStatus.equals(user.userStatus)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 53 * hash + (this.username != null ? this.username.hashCode() : 0);
        hash = 53 * hash + (this.firstName != null ? this.firstName.hashCode() : 0);
        hash = 53 * hash + (this.lastName != null ? this.lastName.hashCode() : 0);
        hash = 53 * hash + (this.email != null ? this.email.hashCode() : 0);
        hash = 53 * hash + (this.password != null ? this.password.hashCode() : 0);
        hash = 53 * hash + (this.phone != null ? this.phone.hashCode() : 0);
        hash = 53 * hash + (this.userStatus != null ? this.userStatus.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", userStatus=" + userStatus +
                '}';
    }
}
