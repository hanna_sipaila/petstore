package com.petstore.model;

public class ApiResponse {
    Integer code;
    String type;
    String message;

    public ApiResponse(){
    }

    public ApiResponse(int code, String type, String message){
        this.code = code;
        this.type = type;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        final ApiResponse response = (ApiResponse) obj;
        if ((this.code == null) ? (response.code != null) : !this.code.equals(response.code)) {
            return false;
        }
        if ((this.type == null) ? (response.type != null) : !this.type.equals(response.type)) {
            return false;
        }
        if ((this.message == null) ? (response.message != null) : !this.message.equals(response.message)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.code != null ? this.code.hashCode() : 0);
        hash = 53 * hash + (this.type != null ? this.type.hashCode() : 0);
        hash = 53 * hash + (this.message != null ? this.message.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "ApiResponse{" +
                "code=" + code +
                ", type='" + type + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
