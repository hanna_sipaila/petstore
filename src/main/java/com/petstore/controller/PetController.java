package com.petstore.controller;

import com.petstore.model.pet.Pet;
import com.petstore.utility.Constants;
import com.petstore.utility.testdatamanager.PetDataManager;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

public class PetController{

    private PetDataManager dataManager = new PetDataManager();

    public Response getPet(Long id){
        return when().
                get(Constants.GET_PET_BY_ID, id);
    }

    public Response addPet(Pet pet) throws IOException {
        String json = dataManager.convertObjectToJson(pet);
        return given()
                .contentType(ContentType.JSON)
                .body(json)
                .post(Constants.POST_PET);
    }

    public Response updatePet(Long petId, String name, String status) {
        String params = "name=%s&status=%s";
        return given()
                .accept(ContentType.JSON)
                .contentType(ContentType.URLENC)
                .body(String.format(params, name, status))
                .post(Constants.UPDATE_PET_BY_ID, petId);
    }

    public Response deletePet(Long petId){
        return given()
                .accept(ContentType.JSON)
                .delete(Constants.DELETE_PET_BY_ID, petId);
    }

}
