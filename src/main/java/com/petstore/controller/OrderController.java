package com.petstore.controller;

import com.petstore.model.order.Order;
import com.petstore.utility.Constants;
import com.petstore.utility.testdatamanager.OrderDataManager;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.io.IOException;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

public class OrderController {

    private OrderDataManager dataManager = new OrderDataManager();

    public Response addOrder(Order order) throws IOException {
        return given()
                .contentType(ContentType.JSON)
                .body(dataManager.convertObjectToJson(order))
                .post(Constants.POST_ORDER);
    }

    public Response getOrder(Long id){
        return when().
                get(Constants.GET_ORDER_BY_ID, id);
    }
}
