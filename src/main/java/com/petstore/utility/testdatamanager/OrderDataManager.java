package com.petstore.utility.testdatamanager;

import com.petstore.model.order.Order;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class OrderDataManager extends TestDataManager<Order> {

    @Override
    public Order convertMapToObject(Map<String, String> map) {
        Order order = new Order();
        order.setId(Long.valueOf(map.get("id")));
        order.setPetId(Long.valueOf(map.get("petId")));
        order.setQuantity(Integer.valueOf(map.get("quantity")));
        order.setShipDate(map.get("shipDate"));
        order.setStatus(map.get("status"));
        order.setComplete(Boolean.parseBoolean(map.get("complete")));
        return order;
    }

    @Override
    public List<Order> readObjects() throws IOException {
        return null;
    }

    @Override
    public Order findObjectByName(List<Order> list, String name) {
        return null;
    }

}
