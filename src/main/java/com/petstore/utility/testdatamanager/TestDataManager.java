package com.petstore.utility.testdatamanager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;

//ToDo refactor TestDataManager
public abstract class TestDataManager<T> {

    protected ObjectMapper yamlReader = new ObjectMapper(new YAMLFactory());

    public abstract List<T> readObjects() throws IOException;

    public abstract T findObjectByName(List<T> list, String name);

    public abstract T convertMapToObject(Map<String, String> map);

    public String convertObjectToJson(T object) throws JsonProcessingException {
        ObjectMapper jsonWriter = new ObjectMapper();
        return jsonWriter.writeValueAsString(object);
    }

}
