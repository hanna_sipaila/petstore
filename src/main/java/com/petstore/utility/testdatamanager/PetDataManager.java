package com.petstore.utility.testdatamanager;

import com.fasterxml.jackson.core.type.TypeReference;
import com.petstore.model.pet.Pet;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;


public class PetDataManager extends TestDataManager<Pet> {

    private static String YAML = "/Applications/petstore/src/test/resources/testdata/pet/pet.yaml";

    @Override
    public List<Pet> readObjects() throws IOException {
        return yamlReader.readValue(new File(YAML), new TypeReference<List<Pet>>(){});
    }

    @Override
    public Pet findObjectByName(List<Pet> list, String name) {
        return list.stream().filter(i -> i.getName().equals(name)).findFirst().get();
    }

    @Override
    public Pet convertMapToObject(Map<String, String> map) {
        Pet pet = new Pet();
        pet.setId(Long.valueOf(map.get("id")));
        pet.setName(map.get("name"));
        pet.setStatus(map.get("status"));
        return pet;
    }

}
