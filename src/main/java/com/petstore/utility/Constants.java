package com.petstore.utility;

public class Constants {
    //ToDo set Base url
    public static final String GET_PET_BY_ID = "https://petstore.swagger.io/v2/pet/{id}";
    public static final String POST_PET = "https://petstore.swagger.io/v2/pet";
    public static final String POST_ORDER = "https://petstore.swagger.io/v2/store/order";
    public static final String UPDATE_PET_BY_ID = "https://petstore.swagger.io/v2/pet/{petId}";
    public static final String DELETE_PET_BY_ID = "https://petstore.swagger.io/v2/pet/{petId}";
    public static final String GET_ORDER_BY_ID = "https://petstore.swagger.io/v2/store/order/{id}";

}
